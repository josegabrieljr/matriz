/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.MatrizFraccion;
import util.Leer_Archivo_Excel;
import view.Operacion_Matriz;
import view.Vista;

/**
 *
 * @author Gabriel Jaimes
 */
public class Controlador {
    
    Vista vista;
    Operacion_Matriz viewMatriz;
    
    private String searchFile() {
        String rutaFile = "";
        //Inicia la herramienta del seleccionador de archivos
        JFileChooser fileExaminer = new JFileChooser();
        //Crea los filtros para que cuando este en el explorador solo le busque el tipo de archivo que necesite
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos Excel", "xls", "xlsx");
        //Agrega los filtros al explorador
        fileExaminer.setFileFilter(filter);
        int returnVal = fileExaminer.showOpenDialog(null);
        //Verificamos si se selecciono un archivo
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            //Si selecciono y acepto correctamente extraemos la ruta del archivo (La ruta es una ruta completa, osea que va incluido el nombre del archivo que se selecciono)
           rutaFile = fileExaminer.getSelectedFile().getAbsolutePath();
            
        }
        //Retornamos el String con la ruta del archivo que seleccionamos
        return rutaFile;
    }
    
    //Metodo para mostrar la ruta en la vista, en este caso en un JTextField 
    public void getRutaFile(){
       viewMatriz.txtRuta.setText(searchFile());
    }
    

    
    
    
/*
    public static void main(String []args) throws Exception{
    Leer_Archivo_Excel leer= new Leer_Archivo_Excel("data.xls");
    
    MatrizFraccion mf= new MatrizFraccion(leer.leerExcel(0));
    MatrizFraccion mf2= new MatrizFraccion(leer.leerExcel(1));   
    System.out.println(mf.getDiagonalPrincipal());
   
     System.out.println("--------------------------------------------------------------------------------------------------");
    System.out.println(mf.toString());
     System.out.println(mf2.toString());
     
     
     
    }*/
   

}
