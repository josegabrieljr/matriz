package model;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class MatrizFraccion implements OperacionMatriz{

    private Fraccion m[][];

    public MatrizFraccion() {
    }

    public MatrizFraccion(String fracciones_excel[][])  {
//Crear la matriz con el tamaño de las filas fracciones_excel, se deja el espacio en blanco para el espacio de las columnas:
        this.m = new Fraccion[fracciones_excel.length][];
        for (int i = 0; i < this.m.length; i++) {
//Para cada fila de fracciones_excel se debe obtener la cantidad de columnas para esa fila "i" y se crea las columnas:
            int cantColumnas = fracciones_excel[i].length;
            this.m[i] = new Fraccion[cantColumnas];
            /*
Iterar para almacenar en cada columna una fracción,
para este paso es necesario que utilice el método split de la clase String, esté método parte la cadena por un carácter
de tal forma que:
String algo="3/4";
String partes[]=algo.split("/");
El resultado sería un vector de dos posiciones:  partes={"3", "4"};
             */
            for (int j = 0; j < cantColumnas; j++) {
                String parteFraccion[] = fracciones_excel[i][j].split("/");
//Convertir cada parte de la cadena a su correspondiente entero (Envoltorio):
                int numerador = Integer.parseInt(parteFraccion[0]);
                int denominador = Integer.parseInt(parteFraccion[1]);
//Crear el objeto fraccción:
                Fraccion nuevaFraccion = new Fraccion(numerador, denominador);
//Insertar esa nueva fracción a la matriz de fracciones:
                this.m[i][j] = nuevaFraccion;
            }
        }
    }

    @Override
    public String toString() {
        String msg = "";
        for (Fraccion fila[] : this.m) {
            for (Fraccion myF : fila) {
                msg += myF.toString() + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    //Otro toString
    public String toString2() {
        String msg = "";
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < this.m[i].length; j++) {
                msg += this.m[i][j].toString() + "\t ";
            }
            msg = "\n";
        }
        return msg;
    }

    /**
     * Retorna la menor fracción almacenada en la matriz
     *
     * @return un string con la información de la fracción menor
     */
    public String getMenor() {//decorador
        return this.getMenor2().toString();
    }

    private Fraccion getMenor2() {
        Fraccion menor = new Fraccion(this.m[0][0].numerador, this.m[0][0].denominador);
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < this.m[i].length; j++) {
                if (menor.compareTo(this.m[i][j]) == 1) {
                    menor = this.m[i][j];
                }
            }

        }
        return menor;
    }

    /**
     * Suma dos matrices fraccionarios
     *
     * @param dos matriz de fracción 2
     * @return una matriz con la suma resultante
     */
    private MatrizFraccion getSumar2(MatrizFraccion dos) {

        MatrizFraccion mt = new MatrizFraccion();
        mt.m = new Fraccion[this.m.length][];
        for (int i = 0; i < this.m.length; i++) {
            mt.m[i] = new Fraccion[this.m[i].length];
            for (int j = 0; j < this.m[i].length; j++) {

                mt.m[i][j] = this.m[i][j].getSumarFracciones(dos.m[i][j]);
            }
        }
        return mt;
    }

    /**
     * Suma dos matrices de fracionarios
     *
     * @param dos matriz de fracción 2
     * @return un String que representa el toString() de la matriz de suma
     * resultante
     */
    public String getSumar(MatrizFraccion dos) {//decorador
        return this.getSumar2(dos).toString();
    }

    /**
     * Resta dos matrices de fracionarios
     *
     * @param dos matriz de fracción 2
     * @return un String que representa el toString() de la matriz de resta
     * resultante
     */
    /*public String getRestar(MatrizFraccion dos) {//decorador
        return this.getRestar2(dos).toString();
    }*/
    public String getRestar(MatrizFraccion dos) {//decorador
        return this.getRestar2(dos).toString();

    }

    private MatrizFraccion getRestar2(MatrizFraccion dos) {

        MatrizFraccion mf = new MatrizFraccion();
        mf.m = new Fraccion[this.m.length][];
        for (int i = 0; i < dos.m.length; i++) {
            mf.m[i] = new Fraccion[this.m[i].length];
            for (int j = 0; j < dos.m[i].length; j++) {
                mf.m[i][j] = this.m[i][j].getRestarFracciones(dos.m[i][j]);
            }
        }
        return mf;

        /**
         * Crea una matriz de 1xM que representa la diagonal principal
         *
         * @return un String con la matriz que almacena la diagonal principal
         */
    }

    public String getDiagonalPrincipal() {//decorador 

        return this.getDiagPrin().toString();
    }

    /**
     * Obtiene la diagonal principal
     *
     * @return MatrizFraccion de tamaño 1xM
     */
    private MatrizFraccion getDiagPrin() {
        MatrizFraccion mf = new MatrizFraccion();
        for (int i = 0; i < this.m.length; i++) {//filas
            for (int j = 0; j <= i; j++) {//columnas

            }
        }
        mf.getDiagonalPrincipal();
        return mf;
    }

    public boolean getCuadrada() {

        for (int i = 0; i < this.m.length; i++) {
            if (this.m.length != this.m[i].length) {

            }
        }
        return true;
    }

    public boolean rectangular() {
        for (int i = 0; i < this.m.length; i++) {
            if (i != 0 && this.m.length != this.m[i].length && this.m[i - 1].length != this.m[i].length) {

                return false;
            }
        }
        return true;
    }

    @Override
    public String getMultiplicación(Object matriz) {
        MatrizFraccion dos = (MatrizFraccion) matriz;
        return this.getMultiplicacion2(dos).toString();

    }

    private MatrizFraccion getMultiplicacion2(MatrizFraccion dos) {

        MatrizFraccion matriz = new MatrizFraccion();
        matriz.m = new Fraccion[this.m.length][dos.m[0].length];
        for (int i = 0; i < this.m.length; i++) {
            Fraccion fr = new Fraccion();
            for (int j = 0; j < this.m[i].length; j++) {

                for (int k = 0; k < this.m[0].length; k++) {
                    if (k == 0) {

                        fr = this.m[i][k].getMultiplicarFracciones(dos.m[k][i]);
                    } else {

                        fr = fr.getSumarFracciones(this.m[i][k].getMultiplicarFracciones(dos.m[k][i]));

                    }
                }
                matriz.m[i][j] = fr;

            }

        }
        return matriz;
    }
    

    @Override
    public String getTranspuesta() {
        return getTranspuesta2().toString();
    }

    private Fraccion[][] getTranspuesta2() {

        MatrizFraccion matriz = new MatrizFraccion();
        matriz.m = new Fraccion[this.m[0].length][this.m.length];
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < this.m[i].length; j++) {
                matriz.m[i][j] = this.m[j][i];
            }
        }
        return matriz.m;
    }

    @Override
    public String getMatrizSimplificada() {
        return "";
    }
    
    private Fraccion [][] getMatrizSimplificada2(){
    return null;
    }

}
