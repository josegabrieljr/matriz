/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Gabriel Jaimes
 */
public class Leer_Archivo_Excel {

    private String nombreArchivo;

    public Leer_Archivo_Excel() {
    }

    public Leer_Archivo_Excel(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String[][] leerExcel(int hojas) throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(this.nombreArchivo));
        //se obtiene la hoja atraves de un indice.
        HSSFSheet hoja = archivoExcel.getSheetAt(hojas);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;
        //Se crea la matriz con la cantidad de filas de la matriz en excel
        String[][] m = new String[canFilas][];
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol = filas.getLastCellNum();
            //Por cada fila se crea la cantidad de columnas
            m[i] = new String[cantCol];
            for (int j = 0; j < cantCol; j++) {
                m[i][j] = filas.getCell(j).getStringCellValue();
            }
        }
        return m;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

}
