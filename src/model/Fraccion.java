package model;

public class Fraccion implements Comparable {

    int numerador;
    int denominador;

    public Fraccion() {
        numerador = 0;
        denominador = 0;
    }

    public Fraccion(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.numerador;
        hash = 89 * hash + this.denominador;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Fraccion fraccion2 = (Fraccion) obj;
        return (this.getDecimal() == fraccion2.getDecimal());
    }

    public float getDecimal() {
        return (this.numerador / this.denominador);
    }

    @Override
    public String toString() {
        return this.numerador + "/" + this.denominador;
    }

    @Override
    public int compareTo(Object o) {
        final Fraccion fraccion2 = (Fraccion) o;

        float rta = this.getDecimal() - fraccion2.getDecimal();
        // Version candida-->return (int)(rta);
        if (rta < 0) {
            return -1;
        } else if (rta > 0) {
            return 1;
        } else {
            return 0;
        }

    }

    public Fraccion getSumarFracciones(Fraccion otro) {

        Integer sumaNumerador = this.numerador * otro.denominador + this.denominador * otro.numerador;
        Integer sumaDenominador = this.denominador * otro.denominador;

        return new Fraccion(sumaNumerador, sumaDenominador);

    }

    public Fraccion getRestarFracciones(Fraccion otro) {
        Integer restaNumerador = this.numerador * otro.denominador - this.denominador * otro.numerador;
        Integer restaDenominador = this.denominador - otro.denominador;

        return new Fraccion(restaNumerador, restaDenominador);
    }

    public Fraccion getMultiplicarFracciones(Fraccion otro) {
       Integer multiplicacionNumerador = this.numerador * otro.numerador;
       Integer multiplicacionDenominador = this.denominador * otro.denominador;
        return new Fraccion(multiplicacionNumerador,multiplicacionDenominador);
    }
    
    /*public Fraccion getSimplificarFraccion(){
    
        
    
    }*/
}
